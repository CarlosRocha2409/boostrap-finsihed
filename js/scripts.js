$(document).ready(all);

function all(){
  carousel_btn();
  reservebuttom();
  login();
}

function carousel_btn(){
  $("#mycarousel").carousel({interval:2000});
  $("#carouselbuttom").click(playbuttom);
}
function playbuttom(){
  if($("#carouselbuttom").children("span").hasClass('fa-pause')){
    $("#mycarousel").carousel('pause');
    $("#carouselbuttom").children("span").removeClass('fa-pause');
    $("#carouselbuttom").children("span").addClass('fa-play');
  }else{
    $("#carouselbuttom").children("span").removeClass('fa-play');
    $("#carouselbuttom").children("span").addClass('fa-pause');
    $("#mycarousel").carousel('cycle');
  }
}
function reservebuttom(){
    $("#btn-reserve").click(showmodalreserve);
  }
  function showmodalreserve(){
    $("#modal-reserve").modal("show");
  }

  function login(){
      $("#login-buttom").click(showmodallogin);
    }
    function showmodallogin(){
      $("#loginmodal").modal("show");
    }
