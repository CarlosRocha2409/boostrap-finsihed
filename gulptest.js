const themename = 'Comfusion page';

// You can declare multiple variables with one statement by starting with var and seperate the variables with a comma and span multiple lines.
// Below are all the Gulp Plugins we're using.
const gulp          = require('gulp'),
      autoprefixer  = require('gulp-autoprefixer'),
      browserSync   = require('browser-sync').create(),
      reload        = browserSync.reload,
      sass          = require('gulp-sass'),
      concat        = require('gulp-concat'),
      uglify        = require('gulp-uglify');

const root          = '../' + themename + '/',
      scss          = 'CSS/',
      js            = 'src/js/',
      jsDist        = 'dist/js/';

const styleWatchFiles =  'CSS/*.scss';

const jsSrc = [
       'node_modules/jquery/dist/jquery.slim.min.js',
       'node_modules/popper.js/dist/umd/popper.min.js',
       'node_modules/bootstrap/dist/js/bootstrap.min.js',
       'js/scripts.js',
       ,

];

const cssSrc= [
    'node_modules/bootstrap/dist/css/bootstrap.min.css',
    'node_modules/font-awesome/css/font-awesome.min.css',
    'node_modules/bootstrap-social/bootstrap-social.css',
    'CSS/Styles.css'

];

function css() {
  return gulp.src(cssSrc)
    .pipe(sass({
      outputStyle: 'compressed'
    }).on('error', sass.logError))
    .pipe(autoprefixer('last 2 versions'))
    .pipe(gulp.dest(root, { sourcemaps: '.' }));
}

function editorCSS() {
  return gulp.src( 'CSS/*.scss' )
    .pipe(sass({
      outputStyle: 'expanded'
    }).on('error', sass.logError))
    .pipe(gulp.dest(root + 'dist/css/'));
}

function javascript() {
  return gulp.src(jsSrc)
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest(jsDist));
}

function watch() {
    browserSync.init({
      open: 'external',
      proxy: 'http://localhost/',
    });
    gulp.watch(styleWatchFiles, css);
    gulp.watch(styleWatchFiles, editorCSS);
    gulp.watch(jsSrc, javascript);
    gulp.watch([ jsDist + 'main.js', root + 'style.css']).on('change', reload);
}

exports.css = css;
exports.editorCSS = editorCSS;
exports.javascript = javascript;
exports.watch = watch;

const build = gulp.series(watch);
gulp.task('default', build);
