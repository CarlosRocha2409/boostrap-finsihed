var gulp = require('gulp');
var cssnano = require('gulp-cssnano');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var clean= require('gulp-clean');

var paths={
  css:[
    "CSS/bootstrap.min.css",
    "CSS/font-awesome.min.css",
    "CSS/bootstrap-social.css",
    "CSS/Styles.css",
    "CSS/aos.css"
  ],

  js: [
    "js/jquery.slim.min.js",
    "js/popper.min.js",
    "js/bootstrap.min.js",
    "js/aos.js",
    "js/aos-trigger.js",
    "js/scripts.js",
  ]

}


gulp.task('sass', function(){
   return gulp.src('CSS/Styles.scss')
      .pipe(sass())
      .pipe(cssnano())
      .pipe(gulp.dest('/CSS'));
});

gulp.task('js', function() {
  return gulp.src(paths.js)
    .pipe(uglify({
            compress: {
                hoist_funs: false
            },
            mangle: false,
        }
    ))
    .pipe(concat('main.js'))
    .pipe(gulp.dest('dist/js'));
});


gulp.task('css', function() {
  return gulp.src(paths.css)
    .pipe(concat('main.css'))
    .pipe(cssnano())
    .pipe(gulp.dest('dist/css'));
});


gulp.task("img",function(){
  return gulp.src('img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/img'));
});

gulp.task('fonts', function() {
    return gulp.src('node_modules/font-awesome/fonts/fontawesome-webfont.*')
            .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('copy', function () {
    return gulp.src('*.html')
        .pipe(gulp.dest('dist/'));
});
gulp.task('clean', function () {
    return gulp.src('dist/', {read: false})
        .pipe(clean({allowEmpty:true}));
});


gulp.task('watch', function(){
gulp.watch('CSS/*.scss',gulp.series('sass','css'));
gulp.watch('js/*.js',gulp.series('js') );
});
var all = gulp.series('sass','css','js','img','fonts','copy');


gulp.task('default',all);
